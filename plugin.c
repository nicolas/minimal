#include "minimal.h"

static gboolean
plugin_init (GstPlugin *plugin)
{
  if (!gst_element_register (plugin, "minimal", GST_RANK_NONE, MY_TYPE_MINIMAL)) {
    GST_ERROR ("Failed to register element minimal");
    return FALSE;
  }

  return TRUE;
}

#define PACKAGE "Minimal"
GST_PLUGIN_DEFINE (GST_VERSION_MAJOR, GST_VERSION_MINOR, minimal,
    "Most minimal plugins with in-place fitler", plugin_init, "1.0",
    GST_LICENSE_UNKNOWN, PACKAGE, "None")
