#include "minimal.h"

GST_DEBUG_CATEGORY_STATIC (minimal_debug);
#define GST_CAT_DEFAULT minimal_debug

struct _MyMinimal
{
  GstBaseTransform parent;
};

G_DEFINE_TYPE_WITH_CODE (MyMinimal, my_minimal, GST_TYPE_BASE_TRANSFORM,
    GST_DEBUG_CATEGORY_INIT (minimal_debug, "minimal", 0, "Minimal filter"));

static GstFlowReturn 
my_minimal_transform_ip (GstBaseTransform * trans, GstBuffer * buf)
{
  GST_DEBUG_OBJECT (trans, "%" GST_PTR_FORMAT, buf);
  return GST_FLOW_OK;
}

static void
my_minimal_init (MyMinimal * self)
{
  /* Uncomment to get writable buffers in my_minimal_transform_ip() */
  // gst_base_transform_set_in_place (GST_BASE_TRANSFORM (self), TRUE);
}

static void
my_minimal_class_init (MyMinimalClass * klass)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GstBaseTransformClass *transform_class = GST_BASE_TRANSFORM_CLASS (klass);
  static GstStaticPadTemplate src_templ = {
    "src", GST_PAD_SRC, GST_PAD_ALWAYS, GST_STATIC_CAPS ("ANY")
  };
  static GstStaticPadTemplate sink_templ = {
    "sink", GST_PAD_SINK, GST_PAD_ALWAYS, GST_STATIC_CAPS ("ANY")
  };

  transform_class->transform_ip = my_minimal_transform_ip;

  gst_element_class_add_static_pad_template (element_class, &src_templ);
  gst_element_class_add_static_pad_template (element_class, &sink_templ);
  gst_element_class_set_metadata (element_class, "Minimal in-place filter",
      "Filter", "A minimal in-place filter",
      "Nicolas Dufresne <nicolas@ndufresne.ca>");
}
