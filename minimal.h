#include <gst/base/base.h>

#ifndef _MY_MINIMAL_H_
#define _MY_MINIMAL_H_
G_BEGIN_DECLS

#define MY_TYPE_MINIMAL my_minimal_get_type ()
G_DECLARE_FINAL_TYPE (MyMinimal, my_minimal, MY, MINIMAL, GstBaseTransform)

G_END_DECLS
#endif
